using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerController : MonoBehaviour
{
    public PlayerData pData;
    private Rigidbody rb;
    private PlayerLookScript lookScript;
    private GroundChecker gChecker;
    private Animator anim;
    private Vector3 moveDirection = Vector3.zero;

    private class PlayerState
    {
        public bool isJump;
        public bool isInspect;
        public bool isFire;
        public bool isMove;
    }

    private PlayerState pState;

    void Start()
    {
        lookScript = GetComponent<PlayerLookScript>();
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        gChecker = GetComponentInChildren<GroundChecker>();
    }

    void Update()
    {
        // STUB: Potentially handle movement if player is active/inactive
        if (this.isActiveAndEnabled)
        {
            HandlePlayerMove();
        }
        else
        {
            HandlePlayerInactiveMove();
        }
    }

    private void HandlePlayerMove()
    {
        // Deal with jump first
        ApplyJump();

        // if in air, forbid the player to move like teleport in air
        if (gChecker.isGround)
            ApplyMovement();
        else
            ApplyAirMovement();


    }

    private void HandlePlayerInactiveMove()
    {
        moveDirection = Vector3.zero;
        ApplyMovement();
    }

    private void ApplyJump()
    {
        // Jump, if grounded
        if (Input.GetButtonDown("Jump"))
        {
            if (gChecker.isGround)
            {
                rb.AddForce(Vector3.up * pData.jump, ForceMode.Impulse);
            }
        }
    }

    private void ApplyMovement()
    {
        // Move direction directly from axes in raw data format @xmug
        float deltaX = Input.GetAxisRaw("Horizontal");
        float deltaZ = Input.GetAxisRaw("Vertical");

        // Direction Calculate @xmug
        moveDirection = -1 * lookScript.tag_view.transform.right * deltaZ +
                         -1 * lookScript.tag_view.transform.up * deltaX;
        moveDirection = Vector3.ProjectOnPlane(moveDirection, Vector3.up);

        // Normalize and Multiply @xmug
        moveDirection.Normalize();

        // Walk or Run?
        if (Input.GetKey(KeyCode.LeftShift))
        {
            moveDirection *= pData.runSpeed;
            anim.SetFloat("h_velocity", moveDirection.magnitude / pData.runSpeed, 0.25f, Time.deltaTime);
        }
        else
        {
            moveDirection *= pData.walkSpeed;
            anim.SetFloat("h_velocity", moveDirection.magnitude / pData.runSpeed, 0.1f, Time.deltaTime);
        }

        // Keep Y axis velocity
        moveDirection.y = rb.velocity.y;



        rb.velocity = moveDirection;
    }

    private void ApplyAirMovement()
    {
        // Move direction directly from axes in raw data format @xmug
        float deltaX = Input.GetAxisRaw("Horizontal");
        float deltaZ = Input.GetAxisRaw("Vertical");

        // Direction Calculate @xmug
        moveDirection = -1 * lookScript.tag_view.transform.right * deltaZ +
                         -1 * lookScript.tag_view.transform.up * deltaX;
        moveDirection = Vector3.ProjectOnPlane(moveDirection, Vector3.up);

        // Normalize and Multiply @xmug
        moveDirection.Normalize();
        rb.AddForce(moveDirection * pData.airForce, ForceMode.Force);
    }
}