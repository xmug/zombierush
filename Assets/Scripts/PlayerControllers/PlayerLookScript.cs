using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLookScript : MonoBehaviour
{
    public GameObject tag_view;
    public float minimumVert = -85.0f;
    public float maximumVert = 85.0f;

    public float sens = 10.0f;

    private float _rotationY = 0;

    private void Update()
    {
        // STUB: Potentially handle look if player is active/inactive
        if (this.isActiveAndEnabled)
        {
            CalculateLookRotation();
        }
    }

    // IMPLEMENTATION METHODS

    private void CalculateLookRotation()
    {
        // Standard Camera movement script @xmug
        var tmp_MouseX = Input.GetAxis("Mouse X") * sens;
        var tmp_MouseY = Input.GetAxis("Mouse Y") * sens;

        // Avoid abnormal big view jump;
        if(Mathf.Abs(tmp_MouseY) > maximumVert)
            tmp_MouseY = 0;

        // Detect Max view angle move
        _rotationY += tmp_MouseY;
        _rotationY = Mathf.Clamp(_rotationY, minimumVert, maximumVert);

        // Rotate whole upper part
        if(_rotationY * _rotationY < maximumVert*maximumVert)
            tag_view.transform.Rotate(Vector3.up, tmp_MouseY, Space.Self);
        tag_view.transform.Rotate(Vector3.up, tmp_MouseX, Space.World);
    }
}
