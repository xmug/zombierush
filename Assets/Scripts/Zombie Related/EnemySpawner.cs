using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner instance;

    public GameObject theEnemy;

    public int waveEnemyCount;

    public float distanceToActivateSpawn;
    public float timeBetweenWaves;
    private float waveSpawnTimer;

    public List<Transform> enemySpawnPositions = new List<Transform>();

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        waveSpawnTimer = 0;
    }

    void Update()
    {
        if (waveSpawnTimer > 0)
        {
            waveSpawnTimer -= Time.deltaTime;
        }
        else
        {
            SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {
        List<Transform> closestEnemySpawns = new List<Transform>();

        // find closest spawns
        for (int i = 0; i < enemySpawnPositions.Count; i++)
        {

            if (Vector3.Distance(GameManager.Instance.playerController.gameObject.transform.position, enemySpawnPositions[i].position) < distanceToActivateSpawn)
            {
                closestEnemySpawns.Add(enemySpawnPositions[i]);
            }
        }

        // if player is too far don't spawn enemies
        if (closestEnemySpawns.Count == 0) return;

        for (int i = 0; i < waveEnemyCount; i++)
        {
            Vector3 spawnPosition = closestEnemySpawns[i % closestEnemySpawns.Count].position;
            Instantiate(theEnemy, spawnPosition, Quaternion.identity);
        }

        // restart timer to spawn wave after the delay
        waveSpawnTimer = timeBetweenWaves;
    }
}
