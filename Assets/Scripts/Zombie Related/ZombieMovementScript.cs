using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieMovementScript : MonoBehaviour
{
    NavMeshAgent agent;

    Vector3 targetPosition;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        targetPosition = GameManager.Instance.playerController.gameObject.transform.position;
        GoToTarget();
    }

    private void GoToTarget()
    {
        agent.SetDestination(targetPosition);
    }
}
