using System;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public float hp = 100.0f;
    public float sp = 100.0f;
    public float walkSpeed = 10.0f;
    public float runSpeed = 14.0f;
    public float airForce = 1.0f;
    public float jump = 4.0f;
    public float ammo = 0;
    public float terminalVelocity = 100f;
}
