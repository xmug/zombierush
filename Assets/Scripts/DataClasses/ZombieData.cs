using System;
using UnityEngine;

[Serializable]
public class ZombieData
{
    public float hp = 100.0f;
    public float sp = 100.0f;
    public float speed = 10.0f;
    public float jump = 4.0f;
}
