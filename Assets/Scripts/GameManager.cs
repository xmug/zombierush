using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    private static readonly Object _lock = new Object();
    public static GameManager Instance
    {
        get { return instance; }
        private set{}
    }

    private void Awake()
    {
        if (instance == null)
        {
            lock (_lock)
            {
                if (instance == null)
                    instance = this;
            }
        }
        DontDestroyOnLoad(instance.gameObject);
    }

    public PlayerController playerController;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

}
