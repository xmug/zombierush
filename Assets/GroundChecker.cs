
using System.Linq;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public LayerMask groundLayer;

    void Start()
    {
        if(groundLayer == 0)
            Debug.LogError("Err: GrounderLayer is not set");
    }

    public bool isGround
    {
        get
        {
            return Physics.OverlapSphere(transform.position, 0.25f, groundLayer).Length > 0;
        }
    }
}
